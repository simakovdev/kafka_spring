package com.kafka.demo.service;
public interface ValueSource {
    void generate();
}