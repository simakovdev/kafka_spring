package com.kafka.demo.service;

import com.kafka.demo.model.StringValue;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.util.function.Consumer;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
@AllArgsConstructor
public class DataSenderKafka implements DataSender{
    private final KafkaTemplate<String, StringValue> template;

    private final Consumer<StringValue> sendAsk;

    private final String topic;

    @Override
    public void send(StringValue value) {
        try {
            log.info("value:{}", value);
            template.send(topic, value)
                    .whenComplete(
                            (result, ex) -> {
                                if (ex == null) {
                                    log.info(
                                            "message id:{} was sent, offset:{}",
                                            value.id(),
                                            result.getRecordMetadata().offset());
                                    sendAsk.accept(value);
                                } else {
                                    log.error("message id:{} was not sent", value.id(), ex);
                                }
                            });
        } catch (Exception ex) {
            log.error("send error, value:{}", value, ex);
        }
    }
}
