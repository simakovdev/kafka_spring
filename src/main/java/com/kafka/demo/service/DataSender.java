package com.kafka.demo.service;

import com.kafka.demo.model.StringValue;

public interface DataSender {
    void send(StringValue value);
}